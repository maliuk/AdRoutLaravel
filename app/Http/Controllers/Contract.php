<?php

namespace AdRout\Http\Controllers;

use AdRout\Helpers\AdRout;
use Illuminate\Http\Request;

class Contract extends Controller
{
    public function ReadContracts()
    {
        
        $contracts = AdRout::request("companies/" . AdRout::$companyId . "/contracts");
        \Debugbar::info($contracts);

        return view('contracts', ['contracts' => $contracts]);
    }
}
