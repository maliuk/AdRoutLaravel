<?php

namespace AdRout\Http\Controllers;

use Illuminate\Http\Request;
use AdRout\Helpers\AdRout;
use Mockery\CountValidator\Exception;
use phpDocumentor\Reflection\Types\Integer;

class Offers extends Controller
{
    public function __invoke()
    {
        $data = AdRout::getMyOffers();
        return view('my-offers', ['res' => $data]);
    }

    public function getOffer($CompanyId, $CompanyOfferId)
    {
        $offer = AdRout::getOffer($CompanyId, $CompanyOfferId);
        $company = AdRout::readCompany($CompanyId);
        \Debugbar::info($offer);
        return view('offer', ['offer' => $offer, 'comp' => $company]);
    }

    public function offerRequests()
    {
        $offersReq = AdRout::getOffersRequests();
        \Debugbar::info($offersReq);
        return view('offers-requests', ['offerRequests' => $offersReq]);
    }
    
    public function offerEdit()
    {
        return view('offer-edit');
    }
}
