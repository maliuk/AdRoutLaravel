<?php

namespace AdRout\Http\Controllers;

use AdRout\Helpers\AdRout;
use Illuminate\Http\Request;

class Test extends Controller
{
    public function __invoke()
    {
        $data = AdRout::request('companies/' . AdRout::$companyId);

        return view('test', ['res' => $data]);
    }
}
