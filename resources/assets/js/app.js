
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

/*const app = new Vue({
    el: '#app'
});*/




//require('modernizr-dist/sticky');
require('jquery-ui-dist/jquery-ui');
require('chartjs');
require('jquery.nicescroll');

"use strict";

jQuery(document).ready(function ($) {

    $('body').addClass('loaded');

    /*$(window).load(function () {
        $('body').addClass('loaded');
    });*/


    /*$('#sidebar menu a').click(function (e) {
     e.preventDefault();

     $('#sidebar menu li').removeClass('active');
     $(this).parent().addClass('active');

     $('body').removeClass('loaded');

     if ($('#burger-menu').hasClass('open'))
     $('#burger-menu').click();

     setTimeout(function () {
     $('body').addClass('loaded');
     }, 1300);
     });*/

    // BURGER MENU
    $('#burger-menu').click(function(){

        if ( $(this).hasClass('open') ) {
            toggleMenu();
        }
        else {
            toggleMenu(true);
        }
    });

    new Swipe(document.getElementById("content"), function(event, direction) {
        //event.preventDefault();

        if ($(event.target).parents().hasClass('scrolable-x') || $(event.target).hasClass('scrolable-x'))
            return;

        switch (direction) {
            case "up":
                // Handle Swipe Up
                break;
            case "down":
                // Handle Swipe Down
                break;
            case "left":
                toggleMenu();
                break;
            case "right":
                toggleMenu(true);
                break;
        }
    });


    // Account menu
    $('#account').click(function (e) {
        e.preventDefault();
        $('#account-menu').toggleClass('opened');
    });
    $('#content').click(function () {
        $('#account-menu').removeClass('opened');
    });


    // Iconic checkbox
    $('.checkbox-iconic i').click(function (e) {
        e.preventDefault();
        $(this).toggleClass('active');
    });

    // Nice scroll
    $('.nice-scroll, .scrolable-x, body').niceScroll({theme: "minimal-dark"}).resize();

    // Filter sidebar
    $('.btn-open-filter').click(function (e) {
        e.preventDefault();
        $('body').toggleClass('filter-opened');
    });
    $('#close-filter').click(function (e) {
        e.preventDefault();
        $('body').removeClass('filter-opened');
    });
    $();

    // Slider in filter
    $('.price-filter').append('<span class="min-value"></span>');
    $('.price-filter').append('<span class="max-value"></span>');


    $('.ui-slider').slider({
        range: true,
        min: 0,
        max: 500,
        values: [ 10, 100 ],
        slide: function( event, ui ) {
            //$( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );

            //$('.min-range').attr('data-value', "$" + ui.values[ 0 ]);
            //$('.max-range').attr('data-value', "$" + ui.values[ 1 ]);

            $('.price-filter > .min-value').text("$" + ui.values[ 0 ]);
            $('.price-filter > .max-value').text("$" + ui.values[ 1 ]);

            $('.price-filter > .min-value').css('left', $('.min-range').css('left'));
            $('.price-filter > .max-value').css('left', $('.max-range').css('left'));

            //console.log(ui.position.x);

        }
    });

    $('.ui-slider .ui-slider-handle:first-of-type').addClass('min-range');
    $('.ui-slider .ui-slider-handle:last-of-type').addClass('max-range');

    $('.price-filter > .min-value').text("$" + $( ".ui-slider" ).slider( "values", 0 ));
    $('.price-filter > .max-value').text("$" + $( ".ui-slider" ).slider( "values", 1 ));

    $('.price-filter > .min-value').css('left', $('.min-range').css('left'));
    $('.price-filter > .max-value').css('left', $('.max-range').css('left'));


    // Table Data Collapsed
    $('.table-data-collapsed .collapse').each(function (i) {
        if (i % 2 == 0) {
            $(this).addClass('odd');
        }
    });

    $('.table-data-collapsed .collapse-hide').prev('.collapse').addClass('has-child');

    $('.button-collapse').click(function (e) {
        e.preventDefault();
        $(this).parents('.collapse').toggleClass('collapse-opened');
    });

    // Buttons
    $('.button-disabled').click(function (e) {
        e.preventDefault();
    });



    // Chart.js
    window.chartColors = {
        red: 'rgb(255, 99, 132)',
        orange: 'rgb(255, 159, 64)',
        yellow: 'rgb(255, 205, 86)',
        green: 'rgb(75, 192, 192)',
        blue: 'rgb(54, 162, 235)',
        purple: 'rgb(153, 102, 255)',
        grey: 'rgb(201, 203, 207)'
    };

    (function(global) {
        var Months = [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December'
        ];

        var COLORS = [
            '#56cc5b',
            '#d57b58',
            '#64c2b5',
            '#e0a952',
            '#d47b58',
            '#6ab4e3',
        ];

        var Samples = global.Samples || (global.Samples = {});
        var Color = global.Color;

        Samples.utils = {
            // Adapted from http://indiegamr.com/generate-repeatable-random-numbers-in-js/
            srand: function(seed) {
                this._seed = seed;
            },

            rand: function(min, max) {
                var seed = this._seed;
                min = min === undefined ? 0 : min;
                max = max === undefined ? 1 : max;
                this._seed = (seed * 9301 + 49297) % 233280;
                return min + (this._seed / 233280) * (max - min);
            },

            numbers: function(config) {
                var cfg = config || {};
                var min = cfg.min || 0;
                var max = cfg.max || 1;
                var from = cfg.from || [];
                var count = cfg.count || 8;
                var decimals = cfg.decimals || 8;
                var continuity = cfg.continuity || 1;
                var dfactor = Math.pow(10, decimals) || 0;
                var data = [];
                var i, value;

                for (i = 0; i < count; ++i) {
                    value = (from[i] || 0) + this.rand(min, max);
                    if (this.rand() <= continuity) {
                        data.push(Math.round(dfactor * value) / dfactor);
                    } else {
                        data.push(null);
                    }
                }

                return data;
            },

            labels: function(config) {
                var cfg = config || {};
                var min = cfg.min || 0;
                var max = cfg.max || 100;
                var count = cfg.count || 8;
                var step = (max - min) / count;
                var decimals = cfg.decimals || 8;
                var dfactor = Math.pow(10, decimals) || 0;
                var prefix = cfg.prefix || '';
                var values = [];
                var i;

                for (i = min; i < max; i += step) {
                    values.push(prefix + Math.round(dfactor * i) / dfactor);
                }

                return values;
            },

            months: function(config) {
                var cfg = config || {};
                var count = cfg.count || 12;
                var section = cfg.section;
                var values = [];
                var i, value;

                for (i = 0; i < count; ++i) {
                    value = Months[Math.ceil(i) % 12];
                    values.push(value.substring(0, section));
                }

                return values;
            },

            color: function(index) {
                return COLORS[index % COLORS.length];
            },

            transparentize: function(color, opacity) {
                var alpha = opacity === undefined ? 0.5 : 1 - opacity;
                return Color(color).alpha(alpha).rgbString();
            }
        };

        // DEPRECATED
        window.randomScalingFactor = function() {
            return Math.round(Samples.utils.rand(0, 1000));
        };

        // INITIALIZATION

        Samples.utils.srand(Date.now());

    }(this));

    var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var config = {
        type: 'line',
        data: {
            labels: ["January", "February", "March", "April", "May", "June", "July"],
            /*datasets: [{
             label: "My First dataset",
             backgroundColor: window.chartColors.red,
             borderColor: window.chartColors.red,
             data: [
             randomScalingFactor(),
             randomScalingFactor(),
             randomScalingFactor(),
             randomScalingFactor(),
             randomScalingFactor(),
             randomScalingFactor(),
             randomScalingFactor()
             ],
             borderWidth: 2,
             fill: false,
             }, {
             label: "My Second dataset",
             fill: false,
             backgroundColor: window.chartColors.blue,
             borderColor: window.chartColors.blue,
             data: [
             randomScalingFactor(),
             randomScalingFactor(),
             randomScalingFactor(),
             randomScalingFactor(),
             randomScalingFactor(),
             randomScalingFactor(),
             randomScalingFactor()
             ],
             borderWidth: 2
             }]*/
        },
        options: {
            responsive: true,
            title:{
                display: false,
                text:'Chart.js Line Chart'
            },
            legend: {
                display: false
            },
            elements: {
                point: {
                    radius: 1
                }
            },
            layout: {
                padding: {
                    left: 50,
                    right: 0,
                    top: 50,
                    bottom: 0
                }
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Month'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: false,
                        labelString: 'Value'
                    }
                }]
            }
        }
    };

    //window.onload = function() {
    var chartEll = document.getElementById("main-chart");
    if (chartEll) {
        var ctx = chartEll.getContext("2d");
        window.myLine = new Chart(ctx, config);
    }
    //};


    var colorNames = Object.keys(window.chartColors);
    function addDataset(_this, color) {

        var newDataset = {
            label: $(_this).parent().data('label'),
            backgroundColor: color,
            borderColor: color,
            data: [],
            fill: false,
            borderWidth: 2
        };

        for (var index = 0; index < config.data.labels.length; ++index) {
            newDataset.data.push(randomScalingFactor());
        }

        config.data.datasets.push(newDataset);
        window.myLine.update();
    }

    function removeDataset(label) {
        //config.data.datasets.splice(0, 1);
        config.data.datasets = config.data.datasets.filter(function( obj ) {
            return obj.label !== label;
        });
        window.myLine.update();
    }


    // Slidebox
    $('.slidebox').each(function () {
        var color = $(this).data('background');
        if (color)
            $(this).css("background-color", color.toString());
    });
    $('.slidebox input[type="checkbox"]').change(slideboxFunction);
    $('.slidebox input[type="checkbox"]').each(slideboxFunction);

    function slideboxFunction() {
        if(this.checked || $(this).is(':checked')) {
            $(this).parents('.slidebox').removeClass('off');
            addDataset(this, $(this).parent().data("background"));
        }
        else {
            $(this).parents('.slidebox').addClass('off');
            removeDataset($(this).parent().data('label'));
        }
    }


    // Pie Chart
    chartEll = document.getElementById("pie-chart");
    if (chartEll) {
        var pieCanvas = chartEll.getContext("2d");

        //Chart.defaults.global.defaultFontFamily = "Lato";
        //Chart.defaults.global.defaultFontSize = 18;

        var oilData = {
            labels: [
                "192.168.1.0",
                "256.120.5.166",
                "111.254.12.5",
                "542.123.444.121",
                "192.100.12.544"
            ],
            datasets: [
                {
                    data: [133, 86, 52, 51, 50],
                    backgroundColor: [
                        "#FF6384",
                        "#63FF84",
                        "#84FF63",
                        "#8463FF",
                        "#6384FF"
                    ]
                }]
        };

        var pieChart = new Chart(pieCanvas, {
            type: 'pie',
            data: oilData
        });
    }


    // Button back
    $('#button-back').click(function (e) {
        e.preventDefault();
        parent.history.back();
    });

});

function toggleMenu(open) {
    if (open) {
        $('#sidebar').addClass("opened");
        $('#burger-menu').addClass('open');

        $('#content').addClass('menu-opened');
    }
    else {
        $('#sidebar').removeClass("opened");
        $('#burger-menu').removeClass('open');

        $('#content').removeClass('menu-opened');
    }
}

function Swipe(elem, callback) {
    var self = this;
    this.callback = callback;

    function handleEvent(e) {
        self.touchHandler(e);
    }

    elem.addEventListener('touchstart', handleEvent, false);
    elem.addEventListener('touchmove', handleEvent, false);
    elem.addEventListener('touchend', handleEvent, false);
}
Swipe.prototype.touches = {
    "touchstart": {"x":-1, "y":-1},
    "touchmove" : {"x":-1, "y":-1},
    "touchend"  : false,
    "direction" : "undetermined"
};
Swipe.prototype.touchHandler = function (event) {
    var touch;
    if (typeof event !== 'undefined'){
        if (typeof event.touches !== 'undefined') {
            touch = event.touches[0];
            switch (event.type) {
                case 'touchstart':
                case 'touchmove':
                    this.touches[event.type].x = touch.pageX;
                    this.touches[event.type].y = touch.pageY;
                    break;
                case 'touchend':
                    this.touches[event.type] = true;
                    var x = (this.touches.touchstart.x - this.touches.touchmove.x),
                        y = (this.touches.touchstart.y - this.touches.touchmove.y);
                    if (x < 0) x /= -1;
                    if (y < 0) y /= -1;
                    if (x > y) {
                        //this.touches.direction = this.touches.touchstart.x < this.touches.touchmove.x ? "right" : "left";

                        if (this.touches.touchstart.x < this.touches.touchmove.x && this.touches.touchmove.x - this.touches.touchstart.x > 100)
                            this.touches.direction = "right";
                        else if (this.touches.touchstart.x > this.touches.touchmove.x && this.touches.touchstart.x - this.touches.touchmove.x > 100)
                            this.touches.direction = "left";
                    } else
                        this.touches.direction = this.touches.touchstart.y < this.touches.touchmove.y ? "down" : "up";
                    this.callback(event, this.touches.direction);
                    break;
            }
        }
    }
};


function hexToRgb(hex){
    var c;
    if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
        c= hex.substring(1).split('');
        if(c.length== 3){
            c= [c[0], c[0], c[1], c[1], c[2], c[2]];
        }
        c= '0x'+c.join('');
        return 'rgba('+[(c>>16)&255, (c>>8)&255, c&255].join(',')+')';
    }
    throw new Error('Bad Hex');
}