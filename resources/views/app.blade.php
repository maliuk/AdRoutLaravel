<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="theme-color" content="#06578a"/>

    <title>AdRout - @yield('title')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="{{ asset('css/normalize.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">

</head>
<body>

<!-- SIDEBAR -->
<aside id="sidebar">

    <a id="logo" href="/">
        <img src="{{ asset('img/Logo.png') }}"/>
    </a>

    <menu>
        <li class="menu-dashboard {{ (\Request::route()->getName() == 'dashboard') ? 'active' : '' }}">
            <a href="{{ route('dashboard') }}">Dashboard</a>
        </li>

        <li class="menu-buying">
            <a href="#">Buying traffic</a>

            <ul class="sub-menu">
                <li class="{{ (\Request::route()->getName() == 'my-offers') ? 'active' : '' }}"><a href="{{ route('my-offers') }}">My Offers</a></li>
                <li class="{{ (\Request::route()->getName() == 'offers-requests') ? 'active' : '' }}"><a href="{{ route('offers-requests') }}">Offers Requests</a></li>
                <li class="{{ (\Request::route()->getName() == 'contracts') ? 'active' : '' }}"><a href="{{ route('contracts') }}">Contracts</a></li>
            </ul>
        </li>

        <li class="menu-report">
            <a href="#">Report</a>

            <ul class="sub-menu">
                <li><a href="#">Report presets</a></li>
                <li><a href="#">Performance</a></li>
                <li><a href="#">Transactions</a></li>
            </ul>
        </li>

        <li class="menu-finance">
            <a href="#">Finance</a>

            <ul class="sub-menu">
                <li class="{{ (\Request::route()->getName() == 'billing') ? 'active' : '' }}"><a href="{{ route('billing') }}">Billing</a></li>
                <li><a href="#">Invoicing history</a></li>
            </ul>
        </li>

        <li class="menu-settings">
            <a href="#">Settings</a>

            <ul class="sub-menu">
                <li><a href="#">Company settings</a></li>
                <li><a href="#">Users</a></li>
                <li><a href="#">Roles</a></li>
                <li><a href="#">Partners</a></li>
                <li><a href="#">Logs</a></li>
            </ul>
        </li>
    </menu>

    <ul id="footer-menu">
        <li><a href="#">SDK</a></li>
        <li><a href="#">Pricing</a></li>
        <li><a href="#">Tour</a></li>
        <li><a href="#">Support</a></li>
    </ul>

</aside>
<!-- END SIDEBAR -->

<!-- FILTER -->
<div id="filter">

    <a href="#" id="close-filter">
        <i class="icon-cross"></i>
    </a>

    <div id="filter-header">
        Filters
    </div>

    <form class="form-filter-sidebar">

        <div>
            <small class="text-light text-up">Contract settings</small>
            <br />
            <div class="input-wrap">
                <input type="text" class="inputText" value="" required/>
                <span class="floating-label">Contract name</span>
            </div>
        </div>

        <div>
            <div class="input-wrap">
                <input type="text" class="inputText" value="" required/>
                <span class="floating-label">Contract ID</span>
            </div>
        </div>

        <div>
            <div class="input-wrap">
                <input type="text" class="inputText" value="" required/>
                <span class="floating-label">Offer name</span>
            </div>
        </div>


        <div>
            <label for="filter-contract-status">Contract status</label>
            <div class="select-styled">
                <select id="filter-contract-status">
                    <option>All</option>
                    <option>Active</option>
                </select>
            </div>
        </div>

        <div></div>

        <div>
            <small class="text-light text-up">Targeting</small>
            <br />
            <br />
            <label for="filter-country">Country</label>
            <div class="select-styled">
                <select id="filter-country">
                    <option>All</option>
                    <option>Active</option>
                </select>
            </div>
        </div>

        <div>
            <label for="filter-language">Language</label>
            <div class="select-styled">
                <select id="filter-language">
                    <option>All</option>
                    <option>Active</option>
                </select>
            </div>
        </div>

        <div>
            <label for="filter-platform">Platform</label>
            <div class="select-styled">
                <select id="filter-platform">
                    <option>All</option>
                    <option>Active</option>
                </select>
            </div>
        </div>


        <div>
            <label for="filter-device">Device type</label>
            <div class="select-styled">
                <select id="filter-device">
                    <option>All</option>
                    <option>Active</option>
                </select>
            </div>
        </div>

        <div>
            <div class="price-filter">
                <div class="ui-slider"></div>
            </div>
        </div>

        <div>
            <a href="#" class="button">Search</a>
            &nbsp;
            <a href="#" id="reset-filter" class="">Reset filters</a>
        </div>

    </form>
</div>
<!-- END FILTER -->

<div id="main" class="container-fluid">

    <!-- HEADER -->
    <header>

        <div>
            <div id="burger-menu">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>

        <div>
            <div id="avatar" style="background-image: url('{{ $company->Icon }}');">

            </div>
            <span>{{ $company->Name }}</span>
        </div>
        <div>
            <a href="#" id="link_for_advert" class="active">For Advert</a>
            <a href="#" id="link_for_publisher">for Publisher</a>
        </div>

        <div>
            <a id="header-upgrade-button" class="button button-blue" href="#">upgrade</a>

            <a href="#" id="alert">
                <span>3</span>
            </a>

            <a href="#" id="account">
                <span class="account-name">Sean Brown</span>
                <span class="account-avatar" style="background-image: url('{{ asset('img/temp/man.jpeg') }}');"></span>
            </a>

            <div id="account-menu">
                <ul>
                    <li><a href="#">First Company</a></li>
                    <li><a href="#">Second Company</a></li>
                    <li><a href="#">Third Company</a></li>
                    <li><a href="#" class="button">new company</a></li>

                    <li class="separator"></li>

                    <li><a href="#">Subscription</a></li>
                    <li><a href="#" class="button">upgrade</a></li>

                    <li class="separator"></li>

                    <li><a href="#">My details</a></li>
                    <li><a href="#">Account settings</a></li>

                    <li class="separator"></li>

                    <li><a href="#">Log Out</a></li>
                </ul>
            </div>
        </div>

    </header>
    <!-- END HEADER -->

    <!-- CONTENT -->
    <main id="content">
        <div class="circleLoader">
        </div>

        @yield('content')

    </main>
    <!-- END CONTENT -->

</div>

<script src="{{ asset('js/modernizr-3.5.0.min.js') }}"></script>

<script src="{{ asset('js/app.js') }}"></script>

<!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
<script>
    window.ga = function () {
        ga.q.push(arguments)
    };
    ga.q = [];
    ga.l = +new Date;
    ga('create', 'UA-XXXXX-Y', 'auto');
    ga('send', 'pageview')
</script>
<script src="https://www.google-analytics.com/analytics.js" async defer></script>

</body>
</html>
