@extends('app')

@section('title', 'Billing')

@section('content')
    <div class="container-fluid content-body">

        <div class="content-header">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <span class="large">Billing</span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">

                <section class="content-box content-box-white scrolable-x">

                    <table class="table-data table-data-collapsed">
                        <thead>
                        <tr>
                            <th colspan="2">Advertiser name</th>
                            <th></th>
                            <th># Invoice</th>
                            <th>Date of inv.</th>
                            <th>Period</th>
                            <th>All installs</th>
                            <th>Rejected</th>
                            <th>Rewarding</th>
                            <th>Amount</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                        <tbody>


                        <tbody class="collapse">
                        <tr>
                            <td class="small-column">
                                <div class="avatar-image" style="background-image: url('./img/temp/1.jpg');">
                                </div>
                            </td>
                            <td>
                                <a href="#" class="link-name">Wext Media</a>
                                <br/>
                                <span class="small text-light-gray">Advertiser id:1234567</span>
                            </td>
                            <td class="small-column">
                                <a href="#"
                                   class="button button-collapse"></a>
                            </td>
                            <td>
                                <a href="#">8653965</a>
                            </td>
                            <td>
                                13 Feb 17
                            </td>
                            <td>
                                13 Feb 17 - 14 Mar 17
                            </td>
                            <td>
                                -
                            </td>
                            <td>
                                -
                            </td>
                            <td>
                                -
                            </td>
                            <td>
                                -
                            </td>
                            <td><span class="status-pending">Pending traffic approval</span>
                            </td>
                            <td>
                                <a href="#" class="button button-upload"></a>
                                <a href="#" class="button button-download"></a>
                            </td>
                        </tr>
                        </tbody>
                        <tbody class="collapse-hide">
                        <tr>
                            <td class="small-column">
                                <div class="avatar-image" style="background-image: url('./img/temp/5.jpg');">
                                </div>
                            </td>
                            <td>
                                <a href="#" class="link-name">Amber Rodriguez</a>
                                <br/>
                                <span class="small text-light-gray">Offer id:1234567</span>
                            </td>
                            <td class="small-column">
                            </td>
                            <td></td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>
                                -
                            </td>
                            <td>
                                -
                            </td>
                            <td>
                                -
                            </td>
                            <td>
                                -
                            </td>
                            <td><span class="status-pending">Pending traffic approval</span>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="small-column">
                                <div class="avatar-image" style="background-image: url('./img/temp/6.jpg');">
                                </div>
                            </td>
                            <td>
                                <a href="#" class="link-name">Carl Scott</a>
                                <br/>
                                <span class="small text-light-gray">Offer id:1234567</span>
                            </td>
                            <td class="small-column">
                            </td>
                            <td></td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>
                                408
                            </td>
                            <td>
                                577
                            </td>
                            <td>
                                729
                            </td>
                            <td>
                                $1,441.75
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="small-column">
                                <div class="avatar-image" style="background-image: url('./img/temp/6.jpg');">
                                </div>
                            </td>
                            <td>
                                <a href="#" class="link-name">Carl Scott</a>
                                <br/>
                                <span class="small text-light-gray">Offer id:1234567</span>
                            </td>
                            <td class="small-column">
                            </td>
                            <td></td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>
                                408
                            </td>
                            <td>
                                577
                            </td>
                            <td>
                                729
                            </td>
                            <td>
                                $1,441.75
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        </tbody>


                        <tbody class="collapse">
                        <tr>
                            <td class="small-column">
                                <div class="avatar-image" style="background-image: url('./img/temp/2.jpg');">
                                </div>
                            </td>
                            <td>
                                <a href="#" class="link-name">Jesse May</a>
                                <br/>
                                <span class="small text-light-gray">Advertiser id:1234567</span>
                            </td>
                            <td class="small-column">
                                <a href="#"
                                   class="button button-collapse"></a>
                            </td>
                            <td>
                                <a href="#">8653965</a>
                            </td>
                            <td>
                                13 Feb 17
                            </td>
                            <td>
                                13 Feb 17 - 14 Mar 17
                            </td>
                            <td>
                                15
                            </td>
                            <td>
                                5
                            </td>
                            <td>
                                12
                            </td>
                            <td>
                                $57.23
                            </td>
                            <td><span class="status-inactive">Not Paid</span>
                            </td>
                            <td>
                                <a href="#" class="button button-upload"></a>
                                <a href="#" class="button button-download"></a>
                            </td>
                        </tr>
                        </tbody>
                        <tbody class="collapse-hide">
                        <tr>
                            <td class="small-column">
                                <div class="avatar-image" style="background-image: url('./img/temp/5.jpg');">
                                </div>
                            </td>
                            <td>
                                <a href="#" class="link-name">Amber Rodriguez</a>
                                <br/>
                                <span class="small text-light-gray">Offer id:1234567</span>
                            </td>
                            <td class="small-column">
                            </td>
                            <td></td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>
                                -
                            </td>
                            <td>
                                -
                            </td>
                            <td>
                                -
                            </td>
                            <td>
                                -
                            </td>
                            <td><span class="status-pending">Pending traffic approval</span>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="small-column">
                                <div class="avatar-image" style="background-image: url('./img/temp/6.jpg');">
                                </div>
                            </td>
                            <td>
                                <a href="#" class="link-name">Carl Scott</a>
                                <br/>
                                <span class="small text-light-gray">Offer id:1234567</span>
                            </td>
                            <td class="small-column">
                            </td>
                            <td></td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>
                                408
                            </td>
                            <td>
                                577
                            </td>
                            <td>
                                729
                            </td>
                            <td>
                                $1,441.75
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="small-column">
                                <div class="avatar-image" style="background-image: url('./img/temp/6.jpg');">
                                </div>
                            </td>
                            <td>
                                <a href="#" class="link-name">Carl Scott</a>
                                <br/>
                                <span class="small text-light-gray">Offer id:1234567</span>
                            </td>
                            <td class="small-column">
                            </td>
                            <td></td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>
                                408
                            </td>
                            <td>
                                577
                            </td>
                            <td>
                                729
                            </td>
                            <td>
                                $1,441.75
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        </tbody>


                        <tbody class="collapse">
                        <tr>
                            <td class="small-column">
                                <div class="avatar-image" style="background-image: url('./img/temp/3.jpg');">
                                </div>
                            </td>
                            <td>
                                <a href="#" class="link-name">Richard Ruiz</a>
                                <br/>
                                <span class="small text-light-gray">Advertiser id:1234567</span>
                            </td>
                            <td class="small-column">
                                <a href="#"
                                   class="button button-collapse"></a>
                            </td>
                            <td>
                                <a href="#" class="button button-plus2"></a>
                            </td>
                            <td>
                                13 Feb 17
                            </td>
                            <td>
                                13 Feb 17 - 14 Mar 17
                            </td>
                            <td>
                                33
                            </td>
                            <td>
                                12
                            </td>
                            <td>
                                12
                            </td>
                            <td>
                                $44.23
                            </td>
                            <td><span class="status-active">Paid</span>
                            </td>
                            <td>
                                <a href="#" class="button button-upload"></a>
                                <a href="#" class="button button-download"></a>
                            </td>
                        </tr>
                        </tbody>
                        <tbody class="collapse-hide">
                        <tr>
                            <td class="small-column">
                                <div class="avatar-image" style="background-image: url('./img/temp/5.jpg');">
                                </div>
                            </td>
                            <td>
                                <a href="#" class="link-name">Amber Rodriguez</a>
                                <br/>
                                <span class="small text-light-gray">Offer id:1234567</span>
                            </td>
                            <td class="small-column">
                            </td>
                            <td></td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>
                                -
                            </td>
                            <td>
                                -
                            </td>
                            <td>
                                -
                            </td>
                            <td>
                                -
                            </td>
                            <td><span class="status-pending">Pending traffic approval</span>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="small-column">
                                <div class="avatar-image" style="background-image: url('./img/temp/6.jpg');">
                                </div>
                            </td>
                            <td>
                                <a href="#" class="link-name">Carl Scott</a>
                                <br/>
                                <span class="small text-light-gray">Offer id:1234567</span>
                            </td>
                            <td class="small-column">
                            </td>
                            <td></td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>
                                408
                            </td>
                            <td>
                                577
                            </td>
                            <td>
                                729
                            </td>
                            <td>
                                $1,441.75
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="small-column">
                                <div class="avatar-image" style="background-image: url('./img/temp/6.jpg');">
                                </div>
                            </td>
                            <td>
                                <a href="#" class="link-name">Carl Scott</a>
                                <br/>
                                <span class="small text-light-gray">Offer id:1234567</span>
                            </td>
                            <td class="small-column">
                            </td>
                            <td></td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>
                                408
                            </td>
                            <td>
                                577
                            </td>
                            <td>
                                729
                            </td>
                            <td>
                                $1,441.75
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        </tbody>


                        <tbody class="collapse">
                        <tr>
                            <td class="small-column">
                                <div class="avatar-image" style="background-image: url('./img/temp/8.jpg');">
                                </div>
                            </td>
                            <td>
                                <a href="#" class="link-name">Samuel Ruiz</a>
                                <br/>
                                <span class="small text-light-gray">Advertiser id:1234567</span>
                            </td>
                            <td class="small-column">
                                <a href="#"
                                   class="button button-collapse"></a>
                            </td>
                            <td>
                                <a href="#">8653965</a>
                            </td>
                            <td>
                                13 Feb 17
                            </td>
                            <td>
                                13 Feb 17 - 14 Mar 17
                            </td>
                            <td>
                                3
                            </td>
                            <td>
                                15
                            </td>
                            <td>
                                67
                            </td>
                            <td>
                                $102.66
                            </td>
                            <td><span class="status-pending">Pending traffic approval</span>
                            </td>
                            <td>
                                <a href="#" class="button button-upload"></a>
                                <a href="#" class="button button-download"></a>
                            </td>
                        </tr>
                        </tbody>
                        <tbody class="collapse-hide">
                        <tr>
                            <td class="small-column">
                                <div class="avatar-image" style="background-image: url('./img/temp/5.jpg');">
                                </div>
                            </td>
                            <td>
                                <a href="#" class="link-name">Amber Rodriguez</a>
                                <br/>
                                <span class="small text-light-gray">Offer id:1234567</span>
                            </td>
                            <td class="small-column">
                            </td>
                            <td></td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>
                                -
                            </td>
                            <td>
                                -
                            </td>
                            <td>
                                -
                            </td>
                            <td>
                                -
                            </td>
                            <td><span class="status-pending">Pending traffic approval</span>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="small-column">
                                <div class="avatar-image" style="background-image: url('./img/temp/6.jpg');">
                                </div>
                            </td>
                            <td>
                                <a href="#" class="link-name">Carl Scott</a>
                                <br/>
                                <span class="small text-light-gray">Offer id:1234567</span>
                            </td>
                            <td class="small-column">
                            </td>
                            <td></td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>
                                408
                            </td>
                            <td>
                                577
                            </td>
                            <td>
                                729
                            </td>
                            <td>
                                $1,441.75
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="small-column">
                                <div class="avatar-image" style="background-image: url('./img/temp/6.jpg');">
                                </div>
                            </td>
                            <td>
                                <a href="#" class="link-name">Carl Scott</a>
                                <br/>
                                <span class="small text-light-gray">Offer id:1234567</span>
                            </td>
                            <td class="small-column">
                            </td>
                            <td></td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>
                                408
                            </td>
                            <td>
                                577
                            </td>
                            <td>
                                729
                            </td>
                            <td>
                                $1,441.75
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        </tbody>


                        </tbody>


                    </table>

                </section>

            </div>

        </div>
    </div>
@endsection