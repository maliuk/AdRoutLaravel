@extends('app')

@section('title', 'Contracts')

@section('content')
    <div class="container-fluid content-body">

        <div class="content-header">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <a class="button btn-open-filter" href="#"><i class="icon-filter"></i> Filters <i
                                class="icon-arrow-slider-right"></i></a>
                    &nbsp;
                    &nbsp;
                    <span class="large">Contracts</span>

                    <a href="#" class="button float-right"><i class="icon-plus"></i> Add new Contract</a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">

                <section class="content-box content-box-white scrolable-x">

                    <table class="table-data">
                        <tr>
                            <th colspan="2">Contract name/Offer name</th>
                            <th colspan="2">Supplier</th>
                            <th>Status</th>
                            <th>Country</th>
                            <th>Lang</th>
                            <th>Platform</th>
                            <th>Device</th>
                            <th>Price</th>
                            <th>Action</th>
                        </tr>

                        @foreach($contracts as $contract)
                            <tr>
                                <td class="small-column">
                                    <div class="avatar-image" style="background-image: url('');">
                                    </div>
                                </td>
                                <td>
                                    <a href="#" class="link-name">{{ $contract->Name }}</a>
                                    <br/>
                                    <span class="small text-light-gray">Contract id: {{ $contract->Id }}</span>
                                    <br/>
                                    <span class="small">{{ $contract->CompanyName }}</span>
                                </td>
                                <td class="small-column">
                                    <div class="avatar-image" style="background-image: url('./img/temp/1.jpg');">
                                    </div>
                                </td>
                                <td>
                                    <a href="#" class="link-name">Sylok Media</a>
                                    <br/>
                                    <span class="small text-light-gray">Offer id:1234567</span>
                                </td>
                                <td>
                                    <span class="status-{{ strtolower($contract->Status) }}">{{ strtolower($contract->Status) }}</span>
                                </td>
                                <td>
                                    US;CA;AE
                                </td>
                                <td>
                                    en;fr
                                </td>
                                <td>
                                    <i class="icon-android-logo"></i>
                                    <i class="icon-apple"></i>
                                </td>
                                <td>
                                    <i class="icon-combine-displays"></i>
                                </td>
                                <td>
                                    1.0
                                </td>
                                <td>
                                    <a href="#" class="button button-report"></a>
                                    <a href="#" class="button button-play"></a>
                                    <a href="#" class="button button-close"></a>
                                </td>
                            </tr>
                        @endforeach

                    </table>

                </section>

            </div>

        </div>
    </div>
@endsection