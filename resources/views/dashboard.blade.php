@extends('app')

@section('title', 'Dashboard')

@section('content')
    <div class="container-fluid content-body">

        <div class="content-header">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <a class="button button-settings2" href="#"></a>
                    &nbsp;
                    &nbsp;
                    <span class="large">Dashboard</span>

                    <a href="#" class="button float-right"><i class="icon-export"></i> Export to CSV</a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">

                <!-- CONTENT SECTION -->
                <section class="content-box content-box-white">
                    <div class="row dashboard-top-info">
                        <div class="col-sm-3">
                            <div class="extra-large">$1256</div>
                            Spent
                        </div>
                        <div class="col-sm-3">
                            <div class="extra-large">120</div>
                            Clicks
                        </div>
                        <div class="col-sm-3">
                            <div class="extra-large">74</div>
                            Installs
                        </div>
                        <div class="col-sm-3">
                            <div class="extra-large">3.5%</div>
                            CR
                        </div>
                    </div>
                </section>
                <!-- END CONTENT SECTION -->


                <!-- CONTENT SECTION -->
                <section class="content-box content-box-white">

                    <div class="row">
                        <div class="col-md-5">

                            <table class="chart-filter-table">

                                <thead>
                                <th></th>
                                <th></th>
                                <th>Today</th>
                                <th>Yesterday</th>
                                </thead>

                                <tbody>
                                <tr>
                                    <td>
                                        <div class="slidebox" data-background="#00becd" data-label="Spent ($)">
                                            <input type="checkbox" id="slidebox1" checked />
                                            <label for="slidebox1"></label>
                                        </div>
                                    </td>
                                    <td>
                                        Spent ($)
                                    </td>
                                    <td class="large">
                                        $1 256.54
                                    </td>
                                    <td class="large text-light">
                                        $1 143.54
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <div class="slidebox" data-background="#56cc5b" data-label="Clicks">
                                            <input type="checkbox" id="slidebox2" checked />
                                            <label for="slidebox2"></label>
                                        </div>
                                    </td>
                                    <td>
                                        Clicks
                                    </td>
                                    <td class="large">120</td>
                                    <td class="large text-light">132</td>
                                </tr>

                                <tr>
                                    <td>
                                        <div class="slidebox" data-background="#64c2b5" data-label="CR (%)">
                                            <input type="checkbox" id="slidebox3" checked />
                                            <label for="slidebox3"></label>
                                        </div>
                                    </td>
                                    <td>CR (%)</td>
                                    <td class="large">5.2%</td>
                                    <td class="large text-light">13.5%</td>
                                </tr>

                                <tr>
                                    <td>
                                        <div class="slidebox" data-background="#e0a952" data-label="Delivered Installs">
                                            <input type="checkbox" id="slidebox4" />
                                            <label for="slidebox4"></label>
                                        </div>
                                    </td>
                                    <td>Delivered Installs</td>
                                    <td class="large">17</td>
                                    <td class="large text-light">17</td>
                                </tr>

                                <tr>
                                    <td>
                                        <div class="slidebox" data-background="#d47b58" data-label="Paid Installs">
                                            <input type="checkbox" id="slidebox5" />
                                            <label for="slidebox5"></label>
                                        </div>
                                    </td>
                                    <td>Paid installs</td>
                                    <td class="large">15</td>
                                    <td class="large text-light">15</td>
                                </tr>

                                <tr>
                                    <td>
                                        <div class="slidebox" data-background="#6ab4e3" data-label="Froud suspected">
                                            <input type="checkbox" id="slidebox6" />
                                            <label for="slidebox6"></label>
                                        </div>
                                    </td>
                                    <td>Froud suspected</td>
                                    <td class="large">4</td>
                                    <td class="large text-light">2</td>
                                </tr>
                                </tbody>

                            </table>

                        </div>

                        <div class="col-md-7">
                            <canvas id="main-chart"></canvas>
                        </div>
                    </div>

                </section>
                <!-- END CONTENT SECTION -->

            </div>

            <div class="col-md-6">
                <section class="content-box content-box-white dash-widget scrolable-x">
                    <h3 class="dash-widget-header">My offers</h3>

                    <table class="table-data">
                        <thead>
                        <tr>
                            <th colspan="2">Offer name</th>
                            <th>Installs</th>
                            <th>CR</th>
                            <th>$</th>
                        </tr>
                        </thead>

                        <tbody>
                        <tr>
                            <td class="small-column">
                                <div class="avatar-image" style="background-image: url('./img/temp/3.jpg');">
                                </div>
                            </td>
                            <td>
                                <a href="#" class="link-name">Ice Cream Madagascar</a>
                                <br />
                                <span class="small text-light-gray">Offer id:1234567</span>
                            </td>
                            <td>
                                13
                            </td>
                            <td>
                                18.5%
                            </td>
                            <td>
                                $259.36
                            </td>
                        </tr>
                        <tr>
                            <td class="small-column">
                                <div class="avatar-image" style="background-image: url('./img/temp/3.jpg');">
                                </div>
                            </td>
                            <td>
                                <a href="#" class="link-name">Ice Cream Madagascar</a>
                                <br />
                                <span class="small text-light-gray">Offer id:1234567</span>
                            </td>
                            <td>
                                13
                            </td>
                            <td>
                                18.5%
                            </td>
                            <td>
                                $259.36
                            </td>
                        </tr>
                        <tr>
                            <td class="small-column">
                                <div class="avatar-image" style="background-image: url('./img/temp/3.jpg');">
                                </div>
                            </td>
                            <td>
                                <a href="#" class="link-name">Ice Cream Madagascar</a>
                                <br />
                                <span class="small text-light-gray">Offer id:1234567</span>
                            </td>
                            <td>
                                13
                            </td>
                            <td>
                                18.5%
                            </td>
                            <td>
                                $259.36
                            </td>
                        </tr>
                        <tr>
                            <td class="small-column">
                                <div class="avatar-image" style="background-image: url('./img/temp/3.jpg');">
                                </div>
                            </td>
                            <td>
                                <a href="#" class="link-name">Ice Cream Madagascar</a>
                                <br />
                                <span class="small text-light-gray">Offer id:1234567</span>
                            </td>
                            <td>
                                13
                            </td>
                            <td>
                                18.5%
                            </td>
                            <td>
                                $259.36
                            </td>
                        </tr>
                        <tr>
                            <td class="small-column">
                                <div class="avatar-image" style="background-image: url('./img/temp/3.jpg');">
                                </div>
                            </td>
                            <td>
                                <a href="#" class="link-name">Ice Cream Madagascar</a>
                                <br />
                                <span class="small text-light-gray">Offer id:1234567</span>
                            </td>
                            <td>
                                13
                            </td>
                            <td>
                                18.5%
                            </td>
                            <td>
                                $259.36
                            </td>
                        </tr>
                        <tr>
                            <td class="small-column">
                                <div class="avatar-image" style="background-image: url('./img/temp/3.jpg');">
                                </div>
                            </td>
                            <td>
                                <a href="#" class="link-name">Ice Cream Madagascar</a>
                                <br />
                                <span class="small text-light-gray">Offer id:1234567</span>
                            </td>
                            <td>
                                13
                            </td>
                            <td>
                                18.5%
                            </td>
                            <td>
                                $259.36
                            </td>
                        </tr>
                        <tr>
                            <td class="small-column">
                                <div class="avatar-image" style="background-image: url('./img/temp/3.jpg');">
                                </div>
                            </td>
                            <td>
                                <a href="#" class="link-name">Ice Cream Madagascar</a>
                                <br />
                                <span class="small text-light-gray">Offer id:1234567</span>
                            </td>
                            <td>
                                13
                            </td>
                            <td>
                                18.5%
                            </td>
                            <td>
                                $259.36
                            </td>
                        </tr>
                        <tr>
                            <td class="small-column">
                                <div class="avatar-image" style="background-image: url('./img/temp/3.jpg');">
                                </div>
                            </td>
                            <td>
                                <a href="#" class="link-name">Ice Cream Madagascar</a>
                                <br />
                                <span class="small text-light-gray">Offer id:1234567</span>
                            </td>
                            <td>
                                13
                            </td>
                            <td>
                                18.5%
                            </td>
                            <td>
                                $259.36
                            </td>
                        </tr>
                        <tr>
                            <td class="small-column">
                                <div class="avatar-image" style="background-image: url('./img/temp/3.jpg');">
                                </div>
                            </td>
                            <td>
                                <a href="#" class="link-name">Ice Cream Madagascar</a>
                                <br />
                                <span class="small text-light-gray">Offer id:1234567</span>
                            </td>
                            <td>
                                13
                            </td>
                            <td>
                                18.5%
                            </td>
                            <td>
                                $259.36
                            </td>
                        </tr>
                        <tr>
                            <td class="small-column">
                                <div class="avatar-image" style="background-image: url('./img/temp/3.jpg');">
                                </div>
                            </td>
                            <td>
                                <a href="#" class="link-name">Ice Cream Madagascar</a>
                                <br />
                                <span class="small text-light-gray">Offer id:1234567</span>
                            </td>
                            <td>
                                13
                            </td>
                            <td>
                                18.5%
                            </td>
                            <td>
                                $259.36
                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <div class="dash-widget-footer"></div>
                </section>
            </div>
            <div class="col-md-6">
                <section class="content-box content-box-white scrolable-x">
                    <h3 class="dash-widget-header">My offers</h3>

                    <table class="table-data">
                        <thead>
                        <tr>
                            <th colspan="2">Offer name</th>
                            <th>Installs</th>
                            <th>CR</th>
                            <th>$</th>
                        </tr>
                        </thead>

                        <tbody>
                        <tr>
                            <td class="small-column">
                                <div class="avatar-image" style="background-image: url('./img/temp/3.jpg');">
                                </div>
                            </td>
                            <td>
                                <a href="#" class="link-name">Ice Cream Madagascar</a>
                                <br />
                                <span class="small text-light-gray">Offer id:1234567</span>
                            </td>
                            <td>
                                13
                            </td>
                            <td>
                                18.5%
                            </td>
                            <td>
                                $259.36
                            </td>
                        </tr>
                        <tr>
                            <td class="small-column">
                                <div class="avatar-image" style="background-image: url('./img/temp/3.jpg');">
                                </div>
                            </td>
                            <td>
                                <a href="#" class="link-name">Ice Cream Madagascar</a>
                                <br />
                                <span class="small text-light-gray">Offer id:1234567</span>
                            </td>
                            <td>
                                13
                            </td>
                            <td>
                                18.5%
                            </td>
                            <td>
                                $259.36
                            </td>
                        </tr>
                        <tr>
                            <td class="small-column">
                                <div class="avatar-image" style="background-image: url('./img/temp/3.jpg');">
                                </div>
                            </td>
                            <td>
                                <a href="#" class="link-name">Ice Cream Madagascar</a>
                                <br />
                                <span class="small text-light-gray">Offer id:1234567</span>
                            </td>
                            <td>
                                13
                            </td>
                            <td>
                                18.5%
                            </td>
                            <td>
                                $259.36
                            </td>
                        </tr>
                        <tr>
                            <td class="small-column">
                                <div class="avatar-image" style="background-image: url('./img/temp/3.jpg');">
                                </div>
                            </td>
                            <td>
                                <a href="#" class="link-name">Ice Cream Madagascar</a>
                                <br />
                                <span class="small text-light-gray">Offer id:1234567</span>
                            </td>
                            <td>
                                13
                            </td>
                            <td>
                                18.5%
                            </td>
                            <td>
                                $259.36
                            </td>
                        </tr>
                        <tr>
                            <td class="small-column">
                                <div class="avatar-image" style="background-image: url('./img/temp/3.jpg');">
                                </div>
                            </td>
                            <td>
                                <a href="#" class="link-name">Ice Cream Madagascar</a>
                                <br />
                                <span class="small text-light-gray">Offer id:1234567</span>
                            </td>
                            <td>
                                13
                            </td>
                            <td>
                                18.5%
                            </td>
                            <td>
                                $259.36
                            </td>
                        </tr>
                        <tr>
                            <td class="small-column">
                                <div class="avatar-image" style="background-image: url('./img/temp/3.jpg');">
                                </div>
                            </td>
                            <td>
                                <a href="#" class="link-name">Ice Cream Madagascar</a>
                                <br />
                                <span class="small text-light-gray">Offer id:1234567</span>
                            </td>
                            <td>
                                13
                            </td>
                            <td>
                                18.5%
                            </td>
                            <td>
                                $259.36
                            </td>
                        </tr>
                        <tr>
                            <td class="small-column">
                                <div class="avatar-image" style="background-image: url('./img/temp/3.jpg');">
                                </div>
                            </td>
                            <td>
                                <a href="#" class="link-name">Ice Cream Madagascar</a>
                                <br />
                                <span class="small text-light-gray">Offer id:1234567</span>
                            </td>
                            <td>
                                13
                            </td>
                            <td>
                                18.5%
                            </td>
                            <td>
                                $259.36
                            </td>
                        </tr>
                        <tr>
                            <td class="small-column">
                                <div class="avatar-image" style="background-image: url('./img/temp/3.jpg');">
                                </div>
                            </td>
                            <td>
                                <a href="#" class="link-name">Ice Cream Madagascar</a>
                                <br />
                                <span class="small text-light-gray">Offer id:1234567</span>
                            </td>
                            <td>
                                13
                            </td>
                            <td>
                                18.5%
                            </td>
                            <td>
                                $259.36
                            </td>
                        </tr>
                        <tr>
                            <td class="small-column">
                                <div class="avatar-image" style="background-image: url('./img/temp/3.jpg');">
                                </div>
                            </td>
                            <td>
                                <a href="#" class="link-name">Ice Cream Madagascar</a>
                                <br />
                                <span class="small text-light-gray">Offer id:1234567</span>
                            </td>
                            <td>
                                13
                            </td>
                            <td>
                                18.5%
                            </td>
                            <td>
                                $259.36
                            </td>
                        </tr>
                        <tr>
                            <td class="small-column">
                                <div class="avatar-image" style="background-image: url('./img/temp/3.jpg');">
                                </div>
                            </td>
                            <td>
                                <a href="#" class="link-name">Ice Cream Madagascar</a>
                                <br />
                                <span class="small text-light-gray">Offer id:1234567</span>
                            </td>
                            <td>
                                13
                            </td>
                            <td>
                                18.5%
                            </td>
                            <td>
                                $259.36
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </section>
            </div>


            <div class="col-md-6">
                <section class="content-box content-box-white dash-widget scrolable-x">
                    <h3 class="dash-widget-header">Dublicated IPs</h3>

                    <table class="table-data">
                        <thead>
                        <tr>
                            <th>My Offers</th>
                            <th>Installs</th>
                        </tr>
                        </thead>

                        <tbody>
                        <tr>
                            <td>192.143.156.376</td>
                            <td>43</td>
                        </tr>
                        <tr>
                            <td>192.143.156.376</td>
                            <td>43</td>
                        </tr>
                        <tr>
                            <td>192.143.156.376</td>
                            <td>43</td>
                        </tr>
                        <tr>
                            <td>192.143.156.376</td>
                            <td>43</td>
                        </tr>
                        <tr>
                            <td>192.143.156.376</td>
                            <td>43</td>
                        </tr>
                        <tr>
                            <td>192.143.156.376</td>
                            <td>43</td>
                        </tr>
                        <tr>
                            <td>192.143.156.376</td>
                            <td>43</td>
                        </tr>
                        <tr>
                            <td>192.143.156.376</td>
                            <td>43</td>
                        </tr>
                        <tr>
                            <td>192.143.156.376</td>
                            <td>43</td>
                        </tr>
                        <tr>
                            <td>192.143.156.376</td>
                            <td>43</td>
                        </tr>
                        </tbody>
                    </table>

                    <div class="dash-widget-footer"></div>
                </section>
            </div>
            <div class="col-md-6">
                <section class="content-box content-box-white">

                    <h3 class="dash-widget-header">Dublicated IPs chart</h3>
                    <canvas id="pie-chart" width="600" height="400"></canvas>

                </section>
            </div>


        </div>
    </div>
@endsection