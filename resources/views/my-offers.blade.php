@extends('app')

@section('title', 'My Offers')

@section('content')
    <div class="container-fluid content-body">

        <div class="content-header">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <a class="button btn-open-filter" href="#"><i class="icon-filter"></i> Filters <i
                                class="icon-arrow-slider-right"></i></a>
                    &nbsp;
                    &nbsp;
                    <span class="large">My Offers</span>

                    <a href="{{ route('offer-edit') }}" class="button float-right"><i class="icon-plus"></i> Add new
                        Offer</a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">

                <section class="content-box content-box-white scrolable-x">

                    <table class="table-data">
                        <tr>
                            <th colspan="2">Offer name</th>
                            <th>Status</th>
                            <th>Country</th>
                            <th>Lang</th>
                            <th>Platform</th>
                            <th>Device</th>
                            <th>Pay In-Out</th>
                            <th>Contracts</th>
                            <th>Actions</th>
                        </tr>

                        @foreach($res as $offer)
                            <tr>
                                <td class="small-column">
                                    <div class="avatar-image" style="background-image: url('{{  $offer->Icon }}');">
                                    </div>
                                </td>
                                <td>
                                    <a href="{{ route('my-offers') }}/{{ $offer->CompanyId }}/{{ $offer->CompanyOfferId }}"
                                       class="link-name">{{ $offer->Name }}</a>
                                    <br/>
                                    <span class="small text-light-gray">Offer id: {{ $offer->CompanyOfferId }}</span>
                                </td>
                                <td>
                                    <span class="status-{{ strtolower($offer->Status) }}">{{ strtolower($offer->Status) }}</span>
                                </td>
                                <td>
                                    @if (isset($offer->TargetRoutes[0]->GeoLocations) && count( (array)$offer->TargetRoutes[0]->GeoLocations ) > 0)
                                        @foreach($offer->TargetRoutes[0]->GeoLocations as $key => $location)
                                            {{ $key }};
                                        @endforeach
                                    @else
                                        Any
                                    @endif
                                </td>
                                <td>
                                    @if (isset($offer->TargetRoutes[0]->Languages) && count( (array)$offer->TargetRoutes[0]->Languages ) > 0)
                                        @foreach($offer->TargetRoutes[0]->Languages as $key => $lang)
                                            {{ strtolower($lang) }};
                                        @endforeach
                                    @else
                                        Any
                                    @endif
                                </td>
                                <td>
                                    @if (isset($offer->TargetRoutes[0]->Platform))
                                        <i class="icon-{{ strtolower($offer->TargetRoutes[0]->Platform) }}-logo"></i>
                                    @else
                                        Any
                                    @endif
                                </td>
                                <td>
                                    {{--<i class="icon-combine-displays"></i>--}}
                                    {{--<i class="icon-monitor"></i>--}}
                                    <i class="icon-tablet"></i>
                                    <i class="icon-phone"></i>
                                </td>
                                <td>
                                    0.8-1.5
                                </td>
                                <td>
                                    @if (isset($offer->ContractsCount))
                                        {{ $offer->ContractsCount }}
                                    @else
                                        0
                                    @endif
                                </td>
                                <td>
                                    <a href="#" class="button button-report"></a>
                                    <a href="#" class="button button-play"></a>
                                    <a href="#" class="button button-settings"></a>
                                    <a href="#" class="button button-close"></a>
                                </td>
                            </tr>
                        @endforeach

                    </table>

                </section>

            </div>

        </div>
    </div>
@endsection