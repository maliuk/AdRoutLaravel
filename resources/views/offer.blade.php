@extends('app')

@section('title', 'Offer')

@section('content')
    <div class="container-fluid content-body">

        <div class="content-header">
            <div class="row align-items-center">
                <div class="col-lg-8">
                    <a id="button-back" class="button button-back" href="#"></a>
                    &nbsp;
                    &nbsp;
                    <span class="large">{{ $offer->Name }}</span>
                    &nbsp;
                    &nbsp;
                    {{--<small>Offer ID: {{ $offer->CompanyOfferId }}</small>--}}

                    <a href="{{ route('offer-edit') }}" class="button float-right">Edit</a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-8">

                <div class="content-header">
                    <div class="row align-items-center">

                        <div class="col-sm-6">
                            <div class="row align-items-center">
                                <div class="col">
                                    <span class="small">Price</span>
                                    <span class="extra-small text-light-gray">(per install)</span>
                                    &nbsp;&nbsp;&nbsp;<span class="large">$ 1.5</span>
                                </div>
                                <div class="col">
                                    <span class="small">Platform:</span>

                                    {{--<i class="icon-android-logo"></i>
                                    <i class="icon-apple"></i>--}}

                                    @if (isset($offer->TargetRoutes[0]->Platform))
                                        <i class="icon-{{ strtolower($offer->TargetRoutes[0]->Platform) }}-logo"></i>
                                    @else
                                        Any
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="row align-items-center">
                                <div class="col">
                                    <span class="small">Device Type:</span>
                                    &nbsp;
                                    <i class="icon-combine-displays"></i>
                                </div>
                                <div class="col">
                                                <span class="small">
                                                    Incentive traffic<br>
                                                    Rebrokering
                                                </span>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <section class="content-box content-box-white">

                    <table class="content-table">

                        <tr>
                            <td>
                                            <span class="user-avatar"
                                                  style="background-image: url('{{ $offer->Icon }}');">
                                            </span>
                            </td>
                            <td>
                                <a href="#" class="link-name">{{ $offer->Name }} </a>
                                <br/>
                                <small>Advertiser ID: {{ $offer->CompanyOfferId }}</small>
                                <br/>
                                <small>
                                    @if (isset($offer->StartDate)) {{ AdRout::dateFormat($offer->StartDate) }} @endif
                                    -
                                    @if (isset($offer->EndDate)) {{ AdRout::dateFormat($offer->EndDate) }}</small> @endif
                                <br/>

                                <small class="text-up status-{{ strtolower($offer->Status) }}">{{ $offer->Status }}</small>
                            </td>
                        </tr>

                        <tr>
                            <td>Countries</td>
                            <td>@if (isset($offer->TargetRoutes[0]->GeoLocations) && count( (array)$offer->TargetRoutes[0]->GeoLocations ) > 0)
                                    @foreach($offer->TargetRoutes[0]->GeoLocations as $key => $location)
                                        {{ AdRout::getCountryByCode($key) }};
                                    @endforeach
                                @else
                                    Any
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>Languages</td>
                            <td>@if (isset($offer->TargetRoutes[0]->Languages) && count( (array)$offer->TargetRoutes[0]->Languages ) > 0)
                                    @foreach($offer->TargetRoutes[0]->Languages as $key => $lang)
                                        {{ AdRout::getLangByCode($lang) }};
                                    @endforeach
                                @else
                                    Any
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>Target URL</td>
                            <td>{{ $offer->TargetRoutes[0]->TargetUrlTemplate }}</td>
                        </tr>
                        <tr>
                            <td>Description</td>
                            <td>@if (isset($offer->Description)) {{ $offer->Description }} @endif</td>
                        </tr>
                        <tr>
                            <td>Terms & Conditions</td>
                            <td>{{ $offer->TermsAndConditions }}</td>
                        </tr>

                    </table>

                </section>

            </div>

            @if (Route::currentRouteName() == 'offers-requests-details')
                <div class="col-lg-4">

                    <div class="row align-items-center">

                        <div class="col-md-12">

                            <div class="content-box">
                                <a href="#" class="button button-blue button-large">Send Request for Contract</a>
                            </div>

                            {{--<div class="alert">
                                You can’t send a request for Contract.
                                Upgrade your Subscription to have this option
                                <br />
                                <br />
                                <a href="#" class="button button-blue">upgrade subscription</a>
                            </div>--}}
                        </div>

                    </div>

                    <div class="row align-items-center">

                        <div class="col-lg-12">
                            <span class="large">About the Advertiser</span>
                        </div>

                    </div>

                    <section class="content-box">
                        <table class="content-table">

                            <tr>
                                <td>
                                            <span class="user-avatar"
                                                  style="background-image: url('{{ $comp->Icon }}');">
                                            </span>
                                </td>
                                <td>
                                    <a href="#" class="link-name">{{ $comp->Name }}</a> <br/>
                                    <small><a href="{{ $comp->Settings->Website }}"
                                              target="_blank">{{ $comp->Settings->Website }}</a></small>
                                    <br/>
                                    {{ $comp->Settings->ZipCode }}, {{ $comp->Settings->Address }}
                                    , {{ $comp->Settings->City }}, {{ $comp->Settings->Country }}
                                </td>
                            </tr>

                            <tr>
                                <td>Secret key</td>
                                <td>{{ $comp->SecretKey }}</td>
                            </tr>
                            <tr>
                                <td>Supplier ID</td>
                                <td>152637848162</td>
                            </tr>

                            <tr>
                                <td class="extra-small">Manager</td>
                                <td>{{ $comp->Settings->Manager }}</td>
                            </tr>
                            <tr>
                                <td class="extra-small">Phone</td>
                                <td>{{ $comp->Settings->Phone }}</td>
                            </tr>
                            <tr>
                                <td class="extra-small">Email</td>
                                <td>{{ $comp->Settings->Email }}</td>
                            </tr>
                            <tr>
                                <td class="extra-small">Skype</td>
                                <td>{{ $comp->Settings->Skype }}</td>
                            </tr>
                            <tr>
                                <td class="extra-small">Description</td>
                                <td>
                                    @if ( isset($comp->Settings->Description) ) {{ $comp->Settings->Description }} @endif
                                </td>
                            </tr>

                        </table>
                    </section>
                </div>
            @endif

        </div>
    </div>
@endsection