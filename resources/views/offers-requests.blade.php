@extends('app')

@section('title', 'Offers Requests')

@section('content')
    <div class="container-fluid content-body">

        <div class="content-header">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <span class="large">Offer Requests</span>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-md-12">

                <section class="content-box content-box-white scrolable-x">

                    <table class="table-data">
                        <tr>
                            <th colspan="2">Offer name</th>
                            <th colspan="2">Published</th>
                            <th>Status</th>
                            <th>Country</th>
                            <th>Lang</th>
                            <th>Platform</th>
                            <th>Device</th>
                            <th>Price</th>
                            <th>Actions</th>
                        </tr>

                        @foreach ($offerRequests as $offerReq)
                            <tr>
                                <td class="small-column">
                                    <div class="avatar-image" style="background-image: url('{{ $offerReq['offer']->Icon }}');">
                                    </div>
                                </td>
                                <td>
                                    <a href="{{ route('offers-requests') }}/{{ $offerReq['offer']->CompanyId }}/{{ $offerReq['offer']->CompanyOfferId }}" class="link-name">{{ $offerReq['offer']->Name }}</a>
                                    <br/>
                                    <span class="small text-light-gray">Offer id: {{ $offerReq['offer']->CompanyOfferId }}</span>
                                </td>
                                <td class="small-column">
                                    <div class="avatar-image" style="background-image: url('{{ $offerReq['company']->Icon }}');">
                                    </div>
                                </td>
                                <td>
                                    <a href="#" class="link-name">{{ $offerReq['company']->Name }}</a>
                                    <br/>
                                    <span class="small text-light-gray">Advertiser id: {{ $offerReq['company']->Id }}</span>
                                </td>
                                <td>
                                    <span class="status-{{ strtolower($offerReq['offerRequest']->Status) }}">{{ strtolower($offerReq['offerRequest']->Status) }}</span>
                                </td>
                                <td>
                                    @if (isset($offerReq['offer']->TargetRoutes[0]->GeoLocations) && count( (array)$offerReq['offer']->TargetRoutes[0]->GeoLocations ) > 0)
                                        @foreach($offerReq['offer']->TargetRoutes[0]->GeoLocations as $key => $location)
                                            {{ $key }};
                                        @endforeach
                                    @else
                                        Any
                                    @endif
                                </td>
                                <td>
                                    @if (isset($offerReq['offer']->TargetRoutes[0]->Languages) && count( (array)$offerReq['offer']->TargetRoutes[0]->Languages ) > 0)
                                        @foreach($offerReq['offer']->TargetRoutes[0]->Languages as $key => $lang)
                                            {{ strtolower($lang) }};
                                        @endforeach
                                    @else
                                        Any
                                    @endif
                                </td>
                                <td>
                                    @if (isset($offerReq['offer']->TargetRoutes[0]->Platform))
                                        <i class="icon-{{ strtolower($offerReq['offer']->TargetRoutes[0]->Platform) }}-logo"></i>
                                    @else
                                        Any
                                    @endif
                                </td>
                                <td>
                                    <i class="icon-combine-displays"></i>
                                </td>
                                <td>
                                    1.0
                                </td>
                                <td>
                                    <a href="#" class="button button-trash"></a>
                                </td>
                            </tr>
                        @endforeach

                    </table>

                </section>

            </div>

        </div>
    </div>
@endsection