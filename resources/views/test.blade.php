@extends('app')

@section('title', 'Test')

@section('content')


    <div class="container-fluid content-body">

        {{--<pre>
            @php
            print_r($res);
            @endphp
        </pre>--}}

        <div class="content-header">
            <div class="row align-items-center">
                <div class="col-lg-8">
                    <a class="button button-back" href="#"></a>
                    &nbsp;
                    &nbsp;
                    <span class="large">{{$res->Name}}</span>
                    &nbsp;
                    &nbsp;
                    <small>Offer ID:85763598</small>

                    <a href="#" class="button float-right">Edit</a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-8">

                <div class="content-header">
                    <div class="row align-items-center">

                        <div class="col-sm-6">
                            <div class="row align-items-center">
                                <div class="col">
                                    <span class="small">Price</span>
                                    <span class="extra-small text-light-gray">(per install)</span>
                                    &nbsp;&nbsp;&nbsp;<span class="large">$ 1.5</span>
                                </div>
                                <div class="col">
                                    <span class="small">Platform:</span>
                                    <i class="icon-android-logo"></i>
                                    <i class="icon-apple"></i>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="row align-items-center">
                                <div class="col">
                                    <span class="small">Device Type:</span>
                                    &nbsp;
                                    <i class="icon-combine-displays"></i>
                                </div>
                                <div class="col">
                                                <span class="small">
                                                    Incentive traffic<br>
                                                    Rebrokering
                                                </span>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <section class="content-box content-box-white">

                    <table class="content-table">

                        <tr>
                            <td>
                                            <span class="user-avatar"
                                                  style="background-image: url('{{$res->Icon}}');">

                                            </span>
                            </td>
                            <td>
                                <a href="#" class="link-name">{{$res->Name}} </a>
                                <br/>
                                <small>Advertiser ID: 46523875</small>
                                <br/>
                                <small>13 Feb 2017 - 14 Mar 2017</small>
                                <br/>
                                <small class="text-up">{{$res->Status}}</small>
                            </td>
                        </tr>

                        <tr>
                            <td>Countries</td>
                            <td>Afghanistan, Albania, Algeria, Andorra, Angola, Anguilla, Antigua & Barbuda,
                                Argentina, Armenia Iran , Iraq, Italy, Ivory Coast (Cote d'Ivoire), Jamaica, Japan,
                                Jordan
                            </td>
                        </tr>
                        <tr>
                            <td>Target URL</td>
                            <td>http://tracking.wextmedia.com/click?aid=6&linkid=B4221</td>
                        </tr>
                        <tr>
                            <td>Description</td>
                            <td>{{$res->Settings->Description}}
                            </td>
                        </tr>
                        <tr>
                            <td>Terms & Conditions</td>
                            <td>We basically consider Zeplin to be our source of truth for collaborating with
                                Engineering. If it’s not in Zeplin, it’s not official.We basically consider Zeplin
                                to be our source of truth for collaborating with Engineering.
                            </td>
                        </tr>

                    </table>

                </section>

            </div>

        </div>
    </div>
@endsection