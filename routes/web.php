<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Dashboard')->name('dashboard');

Route::get('my-offers', 'Offers')->name('my-offers');
Route::get('my-offers/edit/{OfferId?}', 'Offers@offerEdit')->name('offer-edit');
Route::get('my-offers/{CompanyId}/{CompanyOfferId}', 'Offers@getOffer');

Route::get('offers-requests', "Offers@offerRequests")->name('offers-requests');
Route::get('offers-requests/{CompanyId?}/{CompanyOfferId?}', "Offers@getOffer")->name('offers-requests-details');

Route::get('contracts', "Contract@ReadContracts")->name('contracts');

Route::get('billing', function () {
    return view('billing');
})->name('billing');


Route::get('test', 'Test')->name('test');